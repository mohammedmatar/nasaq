import { Component } from '@angular/core';
import { SvgIconRegistryService } from 'angular-svg-icon';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'butterfly';
  constructor(private iconReg: SvgIconRegistryService) {
    this.iconReg.loadSvg('/assets/icons/menu.svg', 'menu');
    this.iconReg.loadSvg('/assets/icons/search.svg', 'search');
    this.iconReg.loadSvg('/assets/icons/arrow-down.svg', 'arrow-down');
    this.iconReg.loadSvg('/assets/icons/play.svg', 'play');
    this.iconReg.loadSvg('/assets/icons/play-colors.svg', 'play-color');
    this.iconReg.loadSvg('/assets/icons/plus.svg', 'plus');
    this.iconReg.loadSvg('/assets/icons/plus-gray.svg', 'plus-gray');
    this.iconReg.loadSvg('/assets/icons/attachment.svg', 'attachment');
  }
}
