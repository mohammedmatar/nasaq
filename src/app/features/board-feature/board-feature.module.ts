import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { BoardFeatureRoutingModule } from './board-feature-routing.module';
import { BoardFeatureComponent } from './board-feature.component';
import { SharedModule } from '@shared/shared.module';
import { BoardService } from '@features/board-feature/board.service';


@NgModule({
  declarations: [BoardFeatureComponent],
  imports: [
    CommonModule,
    BoardFeatureRoutingModule,
    SharedModule,
  ],
  providers: [BoardService]
})
export class BoardFeatureModule { }
