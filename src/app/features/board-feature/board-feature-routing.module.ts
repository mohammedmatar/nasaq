import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { BoardFeatureComponent } from './board-feature.component';

const routes: Routes = [{ path: '', component: BoardFeatureComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class BoardFeatureRoutingModule { }
