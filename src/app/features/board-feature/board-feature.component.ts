import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { IBoard } from '@contracts/Board';
import { BoardService } from '@features/board-feature/board.service';

@Component({
  selector: 'app-board-feature',
  templateUrl: './board-feature.component.html',
  styleUrls: ['./board-feature.component.scss']
})
export class BoardFeatureComponent implements OnInit {
  board$: Observable<IBoard>;
  constructor(private boardService: BoardService) { }

  ngOnInit() {
    this.board$ = this.boardService.one();
  }

}
