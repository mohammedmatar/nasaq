import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BoardFeatureComponent } from './board-feature.component';

describe('BoardFeatureComponent', () => {
  let component: BoardFeatureComponent;
  let fixture: ComponentFixture<BoardFeatureComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BoardFeatureComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BoardFeatureComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
