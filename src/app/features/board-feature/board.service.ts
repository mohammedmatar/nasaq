import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { IBoard } from '@contracts/Board';

@Injectable({
  providedIn: 'root'
})
export class BoardService {

  constructor(private http: HttpClient) { }
  public one(): Observable<IBoard> {
    return this.http.get<IBoard>(`/assets/data/sample-board.json`);
  }
}
