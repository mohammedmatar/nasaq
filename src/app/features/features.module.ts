import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FeaturesRoutingModule } from './features-routing.module';
import { DefaultLayoutComponent } from './+components/default-layout/default-layout.component';
import { SharedModule } from '@shared/shared.module';


@NgModule({
  declarations: [DefaultLayoutComponent],
  imports: [
    CommonModule,
    FeaturesRoutingModule,
    SharedModule
  ]
})
export class FeaturesModule { }
