import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {DefaultLayoutComponent} from '@features/+components/default-layout/default-layout.component';


const routes: Routes = [
  {
    path: 'features',
    component: DefaultLayoutComponent,
    children: [
      { path: 'board', loadChildren: () => import('./board-feature/board-feature.module').then(m => m.BoardFeatureModule) },
      { path: '', redirectTo: 'board', pathMatch: 'full' }
    ]
  }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class FeaturesRoutingModule { }
