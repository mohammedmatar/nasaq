import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {ExceptionLayoutComponent} from './exception-layout/exception-layout.component';
import {NotFoundComponent} from './not-found/not-found.component';
import {ServerErrorComponent} from './server-error/server-error.component';
import {NotAuthorizedComponent} from './not-authorized/not-authorized.component';


const routes: Routes = [
  {
    path: 'exceptions',
    component: ExceptionLayoutComponent, children: [
      { path: '404', component: NotFoundComponent },
      { path: '500', component: ServerErrorComponent },
      { path: '403', component: NotAuthorizedComponent },
      { path: '', redirectTo: '404', pathMatch: 'full' },
      { path: '**', redirectTo: '404', pathMatch: 'full' }
    ],
  },
  { path: '', redirectTo: 'exceptions', pathMatch: 'full' },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ExceptionsRoutingModule {
}
