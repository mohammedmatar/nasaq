import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ExceptionsRoutingModule } from './exceptions-routing.module';
import { NotFoundComponent } from './not-found/not-found.component';
import { ServerErrorComponent } from './server-error/server-error.component';
import { NotAuthorizedComponent } from './not-authorized/not-authorized.component';
import { ExceptionLayoutComponent } from './exception-layout/exception-layout.component';


@NgModule({
  declarations: [NotFoundComponent, ServerErrorComponent, NotAuthorizedComponent, ExceptionLayoutComponent],
  imports: [
    CommonModule,
    ExceptionsRoutingModule
  ]
})
export class ExceptionsModule { }
