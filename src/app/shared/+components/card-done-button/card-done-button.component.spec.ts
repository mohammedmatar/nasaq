import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CardDoneButtonComponent } from './card-done-button.component';

describe('CardDoneButtonComponent', () => {
  let component: CardDoneButtonComponent;
  let fixture: ComponentFixture<CardDoneButtonComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CardDoneButtonComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CardDoneButtonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
