import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-card-done-button',
  templateUrl: './card-done-button.component.html',
  styleUrls: ['./card-done-button.component.scss']
})
export class CardDoneButtonComponent implements OnInit {
  @Input()
  color: string;
  constructor() { }

  ngOnInit() {
  }

}
