import { Component, Input, OnInit } from '@angular/core';
import { ICard } from '@contracts/Card';
import { CardContentTypes } from '@contracts/CardContentType';

@Component({
  selector: 'app-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.scss']
})
export class CardComponent implements OnInit {
  @Input()
  card: ICard;
  @Input()
  color: string;
  ContentTypes = CardContentTypes;
  constructor() { }

  ngOnInit() {
  }

}
