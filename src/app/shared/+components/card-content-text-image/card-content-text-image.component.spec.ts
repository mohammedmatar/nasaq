import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CardContentTextImageComponent } from './card-content-text-image.component';

describe('CardContentTextImageComponent', () => {
  let component: CardContentTextImageComponent;
  let fixture: ComponentFixture<CardContentTextImageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CardContentTextImageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CardContentTextImageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
