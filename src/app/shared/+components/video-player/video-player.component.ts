import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';

@Component({
  selector: 'app-video-player',
  templateUrl: './video-player.component.html',
  styleUrls: ['./video-player.component.scss']
})
export class VideoPlayerComponent implements OnInit {
  @ViewChild('videoPlayer', {static: false}) videoplayer: ElementRef;
  isPlaying = false;
  constructor() { }

  ngOnInit() {
  }

  /**
   * this method will play and pause video.
   * @param event of the current video player @todo: remove it.
   */
  toggleVideo(event: any) {
    (this.videoplayer.nativeElement.paused) ? this.videoplayer.nativeElement.play() : this.videoplayer.nativeElement.pause();
    this.isPlaying = !this.isPlaying;
  }
}
