import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CardTaskMembersComponent } from './card-task-members.component';

describe('CardTaskMembersComponent', () => {
  let component: CardTaskMembersComponent;
  let fixture: ComponentFixture<CardTaskMembersComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CardTaskMembersComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CardTaskMembersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
