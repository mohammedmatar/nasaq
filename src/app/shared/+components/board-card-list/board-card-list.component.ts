import { Component, Input, OnInit } from '@angular/core';
import { IBoardList } from '@contracts/BoardList';

@Component({
  selector: 'app-board-card-list',
  templateUrl: './board-card-list.component.html',
  styleUrls: ['./board-card-list.component.scss']
})
export class BoardCardListComponent implements OnInit {
  @Input()
  boardList: IBoardList;
  constructor() { }

  ngOnInit() {
  }

}
