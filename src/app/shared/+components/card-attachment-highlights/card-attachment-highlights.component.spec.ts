import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CardAttachmentHighlightsComponent } from './card-attachment-highlights.component';

describe('CardAttachmentHighlightsComponent', () => {
  let component: CardAttachmentHighlightsComponent;
  let fixture: ComponentFixture<CardAttachmentHighlightsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CardAttachmentHighlightsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CardAttachmentHighlightsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
