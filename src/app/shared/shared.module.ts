import {ModuleWithProviders, NgModule} from '@angular/core';
import { CommonModule } from '@angular/common';
import { AngularSvgIconModule } from 'angular-svg-icon';
import { HttpClientModule } from '@angular/common/http';
import { HeaderComponent } from '@shared/+components/header/header.component';
import { BoardSwitcherComponent } from '@shared/+components/board-switcher/board-switcher.component';
import { BoardCardListComponent } from '@shared/+components/board-card-list/board-card-list.component';
import { CardComponent } from '@shared/+components/card/card.component';
import { SearchComponent } from '@shared/+components/search/search.component';
import { CardContentTextImageComponent } from '@shared/+components/card-content-text-image/card-content-text-image.component';
import { VideoPlayerComponent } from '@shared/+components/video-player/video-player.component';
import { CardTaskMembersComponent } from '@shared/+components/card-task-members/card-task-members.component';
import { FabComponent } from '@shared/+components/fab/fab.component';
import { CardAttachmentHighlightsComponent } from '@shared/+components/card-attachment-highlights/card-attachment-highlights.component';
import { CardDoneButtonComponent } from '@shared/+components/card-done-button/card-done-button.component';

const SHARED_COMPONENTS = [
  HeaderComponent,
  BoardSwitcherComponent,
  BoardCardListComponent,
  CardComponent,
  SearchComponent,
  CardContentTextImageComponent,
  VideoPlayerComponent,
  CardTaskMembersComponent,
  FabComponent,
  CardAttachmentHighlightsComponent,
  CardDoneButtonComponent
];

@NgModule({
  imports: [
    CommonModule,
    AngularSvgIconModule,
    HttpClientModule,
  ],
  declarations: [
    ...SHARED_COMPONENTS,
  ],
  exports: [
    ...SHARED_COMPONENTS,
  ]
})
export class SharedModule {
  static forRoot(): ModuleWithProviders {
    return {
      ngModule: SharedModule,
      providers: []
    };
  }
}
