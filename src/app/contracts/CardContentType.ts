export enum CardContentTypes {
  TEXT_WITH_IMAGE = 'TEXT_WITH_IMAGE',
  VIDEO = 'VIDEO',
  MEMBERS = 'MEMBERS',
  DONE = 'DONE',
  BOTTOM_FILL_IMAGE = 'BOTTOM_FILL_IMAGE',
}
export interface ICardContentType {
  type: CardContentTypes;
  order: number;
}
export class CardContentType implements ICardContentType {
  order: number;
  type: CardContentTypes;
}
