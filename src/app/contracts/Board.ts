import { BoardList, IBoardList } from '@contracts/BoardList';

/**
 * Board Contract
 */
export interface IBoard {
  title: string;
  desc: string;
  lists: Array<IBoardList>;
}

/**
 * * Board ViewModel Contract
 */
export class Board implements IBoard {
  title: string;
  desc: string;
  lists: Array<BoardList>;
}
