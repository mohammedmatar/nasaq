/**
 * Card Contract
 */
import { CardContentType, ICardContentType } from '@contracts/CardContentType';

export interface ICard {
  title: string;
  has_attachments: boolean;
  order: number;
  children: Array<ICardContentType>;
}

/**
 * Card ViewModel Contract
 */
export class Card implements ICard {
  title: string;
  // tslint:disable-next-line:variable-name
  has_attachments: boolean;
  order: number;
  children: Array<CardContentType>;
}
