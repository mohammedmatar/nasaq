import { Card, ICard } from '@contracts/Card';

/**
 * BoardList Contract
 */
export interface IBoardList {
  title: string;
  color: string;
  order: number;
  cards: Array<ICard>;
}

/**
 * BoardList ViewModel Contract
 */
export class BoardList implements IBoardList {
  title: string;
  color: string;
  order: number;
  cards: Array<Card>;
}
