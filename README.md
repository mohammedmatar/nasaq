# Butterfly
### Live Version
you can access the latest deployed version from here:
[Link to nasaq](http://nasaq.matar.xyz)

[![Netlify Status](https://api.netlify.com/api/v1/badges/a00c366c-af16-4e27-8507-1000649f989b/deploy-status)](https://app.netlify.com/sites/nervous-shockley-4e3802/deploys)
## Documentations
app documentations are auto generated using compodoc,
you can generate them on your local device by running
`yarn docs`. or generate and serve the file by running
`yarn docs:serve` and navigate to [Link to Local Documentations Server](http://localhost:8080).
the documentations are deployed also to live server you can access it from here:
[Link to public Documentations Server](http://nasaq-docs.matar.xyz)
[![Netlify Status](https://api.netlify.com/api/v1/badges/8de6a2e4-42a4-491f-96cd-1282a8dd4777/deploy-status)](https://app.netlify.com/sites/eager-sinoussi-9258bf/deploys).

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).
