'use strict';


customElements.define('compodoc-menu', class extends HTMLElement {
    constructor() {
        super();
        this.isNormalMode = this.getAttribute('mode') === 'normal';
    }

    connectedCallback() {
        this.render(this.isNormalMode);
    }

    render(isNormalMode) {
        let tp = lithtml.html(`
        <nav>
            <ul class="list">
                <li class="title">
                    <a href="index.html" data-type="index-link">Nasaq Task</a>
                </li>

                <li class="divider"></li>
                ${ isNormalMode ? `<div id="book-search-input" role="search"><input type="text" placeholder="Type to search"></div>` : '' }
                <li class="chapter">
                    <a data-type="chapter-link" href="index.html"><span class="icon ion-ios-home"></span>Getting started</a>
                    <ul class="links">
                        <li class="link">
                            <a href="overview.html" data-type="chapter-link">
                                <span class="icon ion-ios-keypad"></span>Overview
                            </a>
                        </li>
                        <li class="link">
                            <a href="index.html" data-type="chapter-link">
                                <span class="icon ion-ios-paper"></span>README
                            </a>
                        </li>
                        <li class="link">
                            <a href="changelog.html"  data-type="chapter-link">
                                <span class="icon ion-ios-paper"></span>CHANGELOG
                            </a>
                        </li>
                        <li class="link">
                            <a href="contributing.html"  data-type="chapter-link">
                                <span class="icon ion-ios-paper"></span>CONTRIBUTING
                            </a>
                        </li>
                        <li class="link">
                            <a href="license.html"  data-type="chapter-link">
                                <span class="icon ion-ios-paper"></span>LICENSE
                            </a>
                        </li>
                        <li class="link">
                            <a href="todo.html"  data-type="chapter-link">
                                <span class="icon ion-ios-paper"></span>TODO
                            </a>
                        </li>
                                <li class="link">
                                    <a href="dependencies.html" data-type="chapter-link">
                                        <span class="icon ion-ios-list"></span>Dependencies
                                    </a>
                                </li>
                    </ul>
                </li>
                    <li class="chapter modules">
                        <a data-type="chapter-link" href="modules.html">
                            <div class="menu-toggler linked" data-toggle="collapse" ${ isNormalMode ?
                                'data-target="#modules-links"' : 'data-target="#xs-modules-links"' }>
                                <span class="icon ion-ios-archive"></span>
                                <span class="link-name">Modules</span>
                                <span class="icon ion-ios-arrow-down"></span>
                            </div>
                        </a>
                        <ul class="links collapse " ${ isNormalMode ? 'id="modules-links"' : 'id="xs-modules-links"' }>
                            <li class="link">
                                <a href="modules/AppModule.html" data-type="entity-link">AppModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-AppModule-927360f001732f0e4c4322d2901b4e0b"' : 'data-target="#xs-components-links-module-AppModule-927360f001732f0e4c4322d2901b4e0b"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-AppModule-927360f001732f0e4c4322d2901b4e0b"' :
                                            'id="xs-components-links-module-AppModule-927360f001732f0e4c4322d2901b4e0b"' }>
                                            <li class="link">
                                                <a href="components/AppComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">AppComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/AppRoutingModule.html" data-type="entity-link">AppRoutingModule</a>
                            </li>
                            <li class="link">
                                <a href="modules/BoardFeatureModule.html" data-type="entity-link">BoardFeatureModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-BoardFeatureModule-f5fa58e5528722a8f4f82661091caa70"' : 'data-target="#xs-components-links-module-BoardFeatureModule-f5fa58e5528722a8f4f82661091caa70"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-BoardFeatureModule-f5fa58e5528722a8f4f82661091caa70"' :
                                            'id="xs-components-links-module-BoardFeatureModule-f5fa58e5528722a8f4f82661091caa70"' }>
                                            <li class="link">
                                                <a href="components/BoardFeatureComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">BoardFeatureComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                                <li class="chapter inner">
                                    <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                        'data-target="#injectables-links-module-BoardFeatureModule-f5fa58e5528722a8f4f82661091caa70"' : 'data-target="#xs-injectables-links-module-BoardFeatureModule-f5fa58e5528722a8f4f82661091caa70"' }>
                                        <span class="icon ion-md-arrow-round-down"></span>
                                        <span>Injectables</span>
                                        <span class="icon ion-ios-arrow-down"></span>
                                    </div>
                                    <ul class="links collapse" ${ isNormalMode ? 'id="injectables-links-module-BoardFeatureModule-f5fa58e5528722a8f4f82661091caa70"' :
                                        'id="xs-injectables-links-module-BoardFeatureModule-f5fa58e5528722a8f4f82661091caa70"' }>
                                        <li class="link">
                                            <a href="injectables/BoardService.html"
                                                data-type="entity-link" data-context="sub-entity" data-context-id="modules" }>BoardService</a>
                                        </li>
                                    </ul>
                                </li>
                            </li>
                            <li class="link">
                                <a href="modules/BoardFeatureRoutingModule.html" data-type="entity-link">BoardFeatureRoutingModule</a>
                            </li>
                            <li class="link">
                                <a href="modules/ExceptionsModule.html" data-type="entity-link">ExceptionsModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-ExceptionsModule-16d90ae2a1ac718bf2405b8c1678e77c"' : 'data-target="#xs-components-links-module-ExceptionsModule-16d90ae2a1ac718bf2405b8c1678e77c"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-ExceptionsModule-16d90ae2a1ac718bf2405b8c1678e77c"' :
                                            'id="xs-components-links-module-ExceptionsModule-16d90ae2a1ac718bf2405b8c1678e77c"' }>
                                            <li class="link">
                                                <a href="components/ExceptionLayoutComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">ExceptionLayoutComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/NotAuthorizedComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">NotAuthorizedComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/NotFoundComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">NotFoundComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/ServerErrorComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">ServerErrorComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/ExceptionsRoutingModule.html" data-type="entity-link">ExceptionsRoutingModule</a>
                            </li>
                            <li class="link">
                                <a href="modules/FeaturesModule.html" data-type="entity-link">FeaturesModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-FeaturesModule-a9714948433fe0d158b0b6cdd7ee9ed0"' : 'data-target="#xs-components-links-module-FeaturesModule-a9714948433fe0d158b0b6cdd7ee9ed0"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-FeaturesModule-a9714948433fe0d158b0b6cdd7ee9ed0"' :
                                            'id="xs-components-links-module-FeaturesModule-a9714948433fe0d158b0b6cdd7ee9ed0"' }>
                                            <li class="link">
                                                <a href="components/DefaultLayoutComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">DefaultLayoutComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/FeaturesRoutingModule.html" data-type="entity-link">FeaturesRoutingModule</a>
                            </li>
                            <li class="link">
                                <a href="modules/SharedModule.html" data-type="entity-link">SharedModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-SharedModule-9a3cb6d257c90a1324022045b794992c"' : 'data-target="#xs-components-links-module-SharedModule-9a3cb6d257c90a1324022045b794992c"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-SharedModule-9a3cb6d257c90a1324022045b794992c"' :
                                            'id="xs-components-links-module-SharedModule-9a3cb6d257c90a1324022045b794992c"' }>
                                            <li class="link">
                                                <a href="components/BoardCardListComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">BoardCardListComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/BoardSwitcherComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">BoardSwitcherComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/CardAttachmentHighlightsComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">CardAttachmentHighlightsComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/CardComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">CardComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/CardContentTextImageComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">CardContentTextImageComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/CardDoneButtonComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">CardDoneButtonComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/CardTaskMembersComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">CardTaskMembersComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/FabComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">FabComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/HeaderComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">HeaderComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/SearchComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">SearchComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/VideoPlayerComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">VideoPlayerComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                </ul>
                </li>
                    <li class="chapter">
                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ? 'data-target="#classes-links"' :
                            'data-target="#xs-classes-links"' }>
                            <span class="icon ion-ios-paper"></span>
                            <span>Classes</span>
                            <span class="icon ion-ios-arrow-down"></span>
                        </div>
                        <ul class="links collapse " ${ isNormalMode ? 'id="classes-links"' : 'id="xs-classes-links"' }>
                            <li class="link">
                                <a href="classes/AppPage.html" data-type="entity-link">AppPage</a>
                            </li>
                            <li class="link">
                                <a href="classes/Board.html" data-type="entity-link">Board</a>
                            </li>
                            <li class="link">
                                <a href="classes/BoardList.html" data-type="entity-link">BoardList</a>
                            </li>
                            <li class="link">
                                <a href="classes/Card.html" data-type="entity-link">Card</a>
                            </li>
                            <li class="link">
                                <a href="classes/CardContentType.html" data-type="entity-link">CardContentType</a>
                            </li>
                        </ul>
                    </li>
                        <li class="chapter">
                            <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ? 'data-target="#injectables-links"' :
                                'data-target="#xs-injectables-links"' }>
                                <span class="icon ion-md-arrow-round-down"></span>
                                <span>Injectables</span>
                                <span class="icon ion-ios-arrow-down"></span>
                            </div>
                            <ul class="links collapse " ${ isNormalMode ? 'id="injectables-links"' : 'id="xs-injectables-links"' }>
                                <li class="link">
                                    <a href="injectables/BoardService.html" data-type="entity-link">BoardService</a>
                                </li>
                            </ul>
                        </li>
                    <li class="chapter">
                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ? 'data-target="#interfaces-links"' :
                            'data-target="#xs-interfaces-links"' }>
                            <span class="icon ion-md-information-circle-outline"></span>
                            <span>Interfaces</span>
                            <span class="icon ion-ios-arrow-down"></span>
                        </div>
                        <ul class="links collapse " ${ isNormalMode ? ' id="interfaces-links"' : 'id="xs-interfaces-links"' }>
                            <li class="link">
                                <a href="interfaces/IBoard.html" data-type="entity-link">IBoard</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/IBoardList.html" data-type="entity-link">IBoardList</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/ICard.html" data-type="entity-link">ICard</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/ICardContentType.html" data-type="entity-link">ICardContentType</a>
                            </li>
                        </ul>
                    </li>
                    <li class="chapter">
                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ? 'data-target="#miscellaneous-links"'
                            : 'data-target="#xs-miscellaneous-links"' }>
                            <span class="icon ion-ios-cube"></span>
                            <span>Miscellaneous</span>
                            <span class="icon ion-ios-arrow-down"></span>
                        </div>
                        <ul class="links collapse " ${ isNormalMode ? 'id="miscellaneous-links"' : 'id="xs-miscellaneous-links"' }>
                            <li class="link">
                                <a href="miscellaneous/enumerations.html" data-type="entity-link">Enums</a>
                            </li>
                            <li class="link">
                                <a href="miscellaneous/variables.html" data-type="entity-link">Variables</a>
                            </li>
                        </ul>
                    </li>
                        <li class="chapter">
                            <a data-type="chapter-link" href="routes.html"><span class="icon ion-ios-git-branch"></span>Routes</a>
                        </li>
                    <li class="chapter">
                        <a data-type="chapter-link" href="coverage.html"><span class="icon ion-ios-stats"></span>Documentation coverage</a>
                    </li>
                    <li class="divider"></li>
                    <li class="copyright">
                        Documentation generated using <a href="https://compodoc.app/" target="_blank">
                            <img data-src="images/compodoc-vectorise.png" class="img-responsive" data-type="compodoc-logo">
                        </a>
                    </li>
            </ul>
        </nav>
        `);
        this.innerHTML = tp.strings;
    }
});